/**
 * 
 */
package golfStore;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;
import items.golfBall;
import items.golfClub;
import items.poloShirt;


/**
 * This ItemList Class uses the items.txt file and maps the data found in it
 * to the two ArrayLists, itemList and priceList and quantityList, and then prints the
 * ArrayList and prompts the user with a choice to add, remove or quit
 * @author jmfin
 *
 */
public class ItemList extends items.TangibleItemTestClass{
	// ArrayLists for storing the product names and prices from items.txt
	// Edit: Now the class only uses one array list and it accepts objects of the 
	// Item class
	// Edit 2: Added separate arrays for shirts, golf balls and golf clubs
	protected ArrayList<Item> itemList;
	protected ArrayList<poloShirt> shirtList;
	protected ArrayList<golfBall> golfBallList;
	protected ArrayList<golfClub> golfClubList;
	
	private Scanner scan = new Scanner(System.in);	
	
	
	/**
	 * Maps the data from the items.txt file to the ArrayLists
	 */
	public void mapItemList() {
		itemList = new ArrayList<Item>();
		shirtList = new ArrayList<poloShirt>();
		golfBallList = new ArrayList<golfBall>();
		golfClubList = new ArrayList<golfClub>();
		
		String currentLine;
		int lineNumber = 0;
		try {			
			File file = new File("items.txt");
			Scanner scan = new Scanner(file);

			//While there are still lines in the file, it adds the first item
			//found to the itemList and the second item to the priceList
			//and then moves to the next line, if possible.
			//Edit: Fixed the Read method, now it splits the lines found in
			//the text file using the tab delimiter and adds Item Class Objects, using the tokenized
			//string for the field inputs, to the itemList array
			while (scan.hasNextLine()) {
				if (scan.hasNextLine()) {
					currentLine = scan.nextLine();
					lineNumber++;
					String[] product = currentLine.split("\t");
					if (product.length == 4) {
						Item item = new Item(product[0], product[1], product[2], product[3]);
						item.setLineNumber(lineNumber);
						if (!item.getPrice().equals("") && !item.getQuantity().equals("")) {
							itemList.add(item);
						}
					} else {
						System.out.println("Incomplete data found at line " + lineNumber + " in the data file");
					}
				}
			}
			scan.close();
		} catch (FileNotFoundException e) {
			System.out.println("An error occurred");
			e.printStackTrace();
		}
		//Edit: Accesses the arrays from TangibleItemsTest and adds the contents
		//to the respective arrays in ItemList
		for (golfBall gb: ItemList.GolfBallItem()) {
			golfBallList.add(gb);
		}
		for (golfClub gc: ItemList.golfClubArray()) {
			golfClubList.add(gc);
		}
		for (poloShirt ps: ItemList.golfShirtsArrayList()) {
			shirtList.add(ps);
		}
	}
	
	/**
	 * Prints out a product list, combining the data from itemList and
	 * priceList
	 */
	public void printList() {
		String itemDescription;
		int itemNumber;
		int choice = 0;
		while (choice != 5) {
			//Edit: Now allows you to view the products by type instead of being displayed all at once
			System.out.println("What list do you wish to view? (1. Generic, 2. Shirts, 3. Clubs, 4. Balls, 5. Go Back)");
			choice = scan.nextInt();
			switch (choice) {
			//For Generic items
			case 1:
				System.out.println("\nGolf Online Store Product List:\n");
				itemNumber = 1;
				//Prints out the name and price of each product
				//Edit: Now uses the Item Class Objects from the itemList to
				//print out the Product list.
				for (Item items: itemList) {
					itemDescription = itemNumber + ". " + items.toString();
					System.out.println(itemDescription);
					itemNumber++;
				}
				System.out.println("\nProduct Count: " + count());
				continue;
			//For shirts
			case 2:

				//Edit: Now prints out the Polo shirt array from Shawn's ShirtTestClass
				itemNumber = 1;
				System.out.println("\nPolo Shirt Options:\n");
				for (poloShirt shirt: shirtList) {
					itemDescription = itemNumber + ". " + shirt.toString();
					System.out.println(itemDescription);
					itemNumber++;
				}
				System.out.println("\nProduct Count: " + shirtCount());
				continue;
			//For golf clubs
			case 3:
				itemNumber = 1;
				//Edit: Since my items.txt file is a list of golf clubs, there will be two lists of golf clubs for now.
				System.out.println("\nGolf Club Options:\n");
				for (golfClub club: golfClubList) {
					itemDescription = itemNumber + ". " + club.toString();
					System.out.println(itemDescription);
					itemNumber++;
				}
				System.out.println("\nProduct Count: " + golfClubCount());
				continue;
			//For golf balls
			case 4:
				itemNumber = 1;
				System.out.println("\nGolf Ball Options:\n");
				for (golfBall ball: golfBallList) {
					itemDescription = itemNumber + ". " + ball.toString();
					System.out.println(itemDescription);
					itemNumber++;
				}
				System.out.println("\nProduct Count: " + golfBallCount());
				continue;
			case 5:
				break;
			default:
				continue;
			}
		}		
	}
	
	/**
	 * Adds a product to the product list and the items file using the
	 * given input
	 * @param itemID Item Identification number
	 * @param quantity Quantity of Product
	 * @param itemName Name of Product
	 * @param price Price of Product
	 * @throws IOException For ignoring io errors
	 */
	public void add(String itemID, String itemName, String price, String quantity) throws IOException {
		//Adds the name and price inputs for the product 
		//to their respective lists
		Item item = new Item(itemID, itemName.replace(' ', '_'), price, quantity);
		itemList.add(item);
		//Adds a new line to the items.txt file using the item name and price
		//given
		String text = itemName.replace(' ', '_') + "\t" + price + "\t" + quantity;
		FileWriter fw = new FileWriter("items.txt", true);
		fw.write("\n" + text);
		fw.close();
	}
	
	/**
	 * For adding shirts to the shirtsList array
	 * Not implemented.
	 */
	public void addShirt() {
		
	}
	
	/**
	 * For adding golf clubs to the golfClubList array
	 * Not implemented.
	 */
	public void addClub() {
		
	}
	
	/**
	 * For adding golf balls to the golfBallList array
	 * Not implemented.
	 */
	public void addBall() {
		
	}
 	
	/**
	 * Removes a product from the product list and items file using
	 * the given input
	 * 
	 * @param itemNumber Item number (not ID or SKU) in the product list
	 * @throws IOException For ignoring io errors
	 */
	public void remove(int itemNumber) throws IOException {
		// For validating the input
		if ((itemNumber > 0 & itemNumber <= itemList.size())) {
			//Creates a string that matches the product's data to be removed in the format used in the data text file
			String removedItem = itemList.get(itemNumber - 1).getProductName().replace(' ', '_') + "\t" + itemList.get(itemNumber - 1).getPrice() + "\t" + itemList.get(itemNumber - 1).getQuantity();
			//Removes the name and price of the selected product from
			//the ArrayLists
			//Edit: Now it only removes the Item Class Object in the chosen index
			if (itemList.get(itemNumber - 1) != null) {
					itemList.remove(itemNumber - 1);
			}
			//Opens the original file and makes a temp file
			File inputFile = new File("items.txt");
			File tempFile = File.createTempFile("itemsTemp", ".txt");
			//Opens the original file in a buffered reader and the temp
			//in a buffered writer
			BufferedReader br = new BufferedReader(new FileReader(inputFile));
			BufferedWriter bw = new BufferedWriter(new FileWriter(tempFile, true));
			//Stores the current line
			String currentLine;
			//Tracks the line number
			int lineCount = 1;
			//Modifies the temp file while there are line remaining in the original
			while ((currentLine = br.readLine()) != null) {
				// If the line count matches the input, the line is skipped,
				// else the current line is written to the temp
				// Edit: Now checks if the current line data matches the item data to
				// be removed. Helps with validation just in case any of the lines before the
				// line that will be removed includes invalid data, of which would cause a line number
				// difference compared to the item number
				if (lineCount == itemNumber) {
					if (currentLine.equals(removedItem)) {
						lineCount++;
						continue;
					} else {
						if (lineCount == 1 || (lineCount == 2 & itemNumber == 1)) {
							bw.write(currentLine);
							continue;
						} else {
							bw.write("\n" + currentLine);
							continue;
						}
					}
				} else {
					if (lineCount == 1 || (lineCount == 2 & itemNumber == 1)) {
						bw.write(currentLine);
						lineCount++;
					} else {
						bw.write("\n" + currentLine);
						lineCount++;
					}	
				}
			}
			bw.close();
			br.close();
			//Deletes the original file
			if (inputFile.delete()) {
				System.out.println("Deletion: Success");
			} else {
				System.out.println("Deletion: Failed");
			}
			//Renames the temp file to match the original
			if (tempFile.renameTo(inputFile)) {
				System.out.println("Renaming Temp File: Success");
			} else {
				System.out.println("Renaming Temp File: Failed");
			}
		} else {
			System.out.println("Invalid Item Number");
		}
	}
	
	/**
	 * Removes a shirt from shirtsList
	 * Currently doesn't do much
	 * @param number Item number in the array
	 */
	public void removeShirt(int number) {
		shirtList.remove(number - 1);
	}
	
	/**
	 * Removes a golf club from golfClubList
	 * Currently doesn't do much
	 * @param number Item number in the array
	 */
	public void removeClub(int number) {
		golfClubList.remove(number - 1);
	}
	
	/**
	 * Removes a golf ball from golfBallList
	 * Currently doesn't do much
	 * @param number Item number in the array
	 */
	public void removeBall(int number) {
		golfBallList.remove(number - 1);
	}
	
	/**
	 * Returns the number of different generic items in the list
	 * @return Number of generic items
	 */
	public int count() {
		return itemList.size();
	}
	
	/**
	 * Returns the number of shirts in the list
	 * @return Number of shirts
	 */
	public int shirtCount() {
		return shirtList.size();
	}
	
	/**
	 * Returns the number of golf clubs in the list
	 * @return Number of golf clubs
	 */
	public int golfClubCount() {
		return golfClubList.size();
	}
	
	/**
	 * Returns the number of golf balls in the list
	 * @return Number of golf balls
	 */
	public int golfBallCount() {
		return golfBallList.size();
	}
	
	/**
	 * Gives the user the choice to add a product, remove a product
	 * or quit the program
	 * @throws IOException For ignoring IO errors
	 */
	public void choices() throws IOException {	
		int input = 0;
		while (input != 3) {
			System.out.println("\nWhat do you wish to do?\n1. Add a Product\n2. Remove a Product\n3. Quit");
			System.out.println("Please input number choice:");
			try {
				input = scan.nextInt();
				scan.nextLine();
			} catch (InputMismatchException e) {
				System.out.println("Invalid Choice");
				continue;
			}

			switch(input) {
			//For Adding a product to itemList (for now)
			case 1:
				System.out.print("Input Product Name: ");
				String itemID;
				String name = scan.nextLine();
				String quantity;
				String price;
				try {
					System.out.print("Input Item ID Number: ");
					itemID = scan.next();
					Integer.parseInt(itemID);
					System.out.print("Input Product Price (Leave out the $): ");
					price = scan.next();
					Double.parseDouble(price);
					System.out.print("Input Product Quantity: ");
					quantity = scan.next();
					Integer.parseInt(quantity);
				} catch (NumberFormatException e) {
					System.out.println("Invalid Input");
					continue;
				}
				add(itemID, name, price, quantity);
				printList();
				System.out.println("Item Count: " + count());
				continue;
			//For removing a product	
			case 2:
				System.out.print("Input Item Number: ");
				try {
					remove(scan.nextInt());
				} catch (InputMismatchException e) {
					System.out.println("Invalid item number");
					continue;
				}
				printList();
				System.out.println("Item Count: " + count());
				continue;
			//To quit the program
			case 3:
				scan.close();
				System.exit(0);
			default:
				continue;
			}
		}
	}
}
