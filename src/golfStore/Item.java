/**
 * 
 */
package golfStore;

/**
 * @author jmfin
 *
 */
public class Item {
	public String itemID;
	public String productName;
	public String price;
	public String quantity;
	public int lineNumber;
	
	/**
	 * Constructor for Item
	 * @param itemID Item Identification Number
	 * @param productName Name of Product
	 * @param productPrice Price of Product
	 * @param productQuantity Quantity of Product
	 */
	public Item(String itemID, String productName, String productPrice, String productQuantity) {
		setItemID(itemID);
		setProductName(productName);
		setPrice(productPrice);
		setQuantity(productQuantity);
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	//Newly added toString method
	@Override
	public String toString() {
		return productName + "\tPrice: $" + price + "\tQuantity: " + quantity + "\tItem ID: " + itemID;
	}
	/**
	 * Returns the Item ID
	 * @return the itemID
	 */
	public String getItemID() {
		try {
			Integer.parseInt(itemID);
			
		} catch (NumberFormatException e) {
			System.out.println("Invalid item ID value found at line " + lineNumber + " in data file");
		}
		return itemID;
	}
	/**
	 * For setting the Item ID
	 * @param itemID the itemID to set
	 */
	public void setItemID(String itemID) {
		this.itemID = itemID;
	}
	/**
	 * Gets the product name
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}
	/**
	 * For setting the product name
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName.replace('_', ' ');
	}
	/**
	 * Returns the product price
	 * @return the price
	 */
	public String getPrice() {
		try {
			Double.parseDouble(price);
		} catch (NumberFormatException e) {
			System.out.println("Invalid price found at line " + lineNumber + " in data file");
			return "";
		}
		return price;
	}
	/**
	 * For setting the product price
	 * @param price the price to set
	 */
	public void setPrice(String price) {
		try {
			Double.parseDouble(price);
			this.price = price;
		} catch (NumberFormatException e) {
			System.out.println("Error occurred");
			this.price = "";
		}
	}
	/**
	 * Returns the quantity of the product
	 * @return the quantity
	 */
	public String getQuantity() {
		try {
			Integer.parseInt(quantity);
			
		} catch (NumberFormatException e) {
			System.out.println("Invalid quantity value found at line " + lineNumber + " in data file");
		}
		return quantity;
	}
	/**
	 * For setting the quantity of the product
	 * @param quantity the quantity to set
	 */
	public void setQuantity(String quantity) {
		try {
			Integer.parseInt(quantity);
			this.quantity = quantity;
		} catch (NumberFormatException e) {
			System.out.println("Error occurred");
			this.quantity = "";
		}
	}
	/**
	 * Returns the current line number in the text file
	 * @return the lineNumber
	 */
	public int getLineNumber() {
		return lineNumber;
	}
	/**
	 * For setting the current line number
	 * @param lineNumber the lineNumber to set
	 */
	public void setLineNumber(int lineNumber) {
		this.lineNumber = lineNumber;
	}
}
