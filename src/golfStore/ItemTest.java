/**
 * 
 */
package golfStore;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author jmfin
 *
 */
public class ItemTest {
	private String itemID = "123456789";
	private String name = "Test Product";
	private String price = "49.99";
	private String quantity = "12";
	
	Item itemTest = new Item(itemID, name, price, quantity);
	@Test
	public void test() {
		assertEquals(itemID, itemTest.getItemID());
		assertEquals(name, itemTest.getProductName());
		assertEquals(price, itemTest.getPrice());
		assertEquals(quantity, itemTest.getQuantity());
	}

}
