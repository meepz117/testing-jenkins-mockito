/**
 * 
 */
package golfStore;

import java.util.ArrayList;

import items.golfBall;
import items.golfClub;
import items.poloShirt;

/**
 * @author jmfin
 *
 */
public class ShoppingCart {
	public ArrayList<Item> shoppingCart = new ArrayList<Item>();
	public ArrayList<poloShirt> shirtsCart = new ArrayList<poloShirt>();
	public ArrayList<golfClub> golfClubCart = new ArrayList<golfClub>();
	public ArrayList<golfBall> golfBallCart = new ArrayList<golfBall>();
	public ItemList item = new ItemList();
	public int itemCount = 0;
	public boolean cartChanged = false;
	
	/**
	 * @return the cartChanged
	 */
	public boolean isCartChanged() {
		return cartChanged;
	}

	/**
	 * @param cartChanged the cartChanged to set
	 */
	public void setCartChanged(boolean cartChanged) {
		this.cartChanged = cartChanged;
	}

	/**
	 * Adds generic items to the shoppingCart array
	 * @param number Item Number (not Item ID or SKU) on the product list
	 * @param quant quantity of the chosen item
	 */
	public void addToCart(int number, String quant) {
		if (number >= 1 && number <= item.itemList.size()) {
			int currentQuantity = Integer.parseInt(item.itemList.get(number - 1).getQuantity());
			if ( currentQuantity > 0 && Integer.parseInt(quant) >= 1 && Integer.parseInt(quant) <= currentQuantity) {
				String newQuantity = "" + (currentQuantity - Integer.parseInt(quant));
				item.itemList.get(number - 1).setQuantity(newQuantity);
				Item itemInCart = new Item(item.itemList.get(number -1).getItemID(), item.itemList.get(number - 1).getProductName(), item.itemList.get(number - 1).getPrice(), item.itemList.get(number - 1).getQuantity());
				itemInCart.setQuantity(quant);
				shoppingCart.add(itemInCart);
				itemCount++;
				cartChanged = true;
			} else {
				System.out.println("Invalid quantity");
			}
		} else {
			System.out.println("Invalid item choice");
		}
				
	}
	
	/**
	 * Adds shirts to the shirtsCart array
	 * @param number Item number on product list
	 */
	public void addShirtToCart(int number) {
		if (item.shirtList.get(number - 1) != null) {
			shirtsCart.add(item.shirtList.get(number - 1));
			itemCount++;
			cartChanged = true;
		}	
	}
	
	/**
	 * Adds golf clubs to the golfClubCart array
	 * @param number Item number on product list
	 */
	public void addClubToCart(int number) {
		if (item.golfClubList.get(number - 1) != null) {
			golfClubCart.add(item.golfClubList.get(number - 1));
			itemCount++;
			cartChanged = true;
		}	
	}
	
	/**
	 * Adds golf balls to the golfBallCart array
	 * @param number Item number on product list
	 */
	public void addBallToCart(int number) {
		if (item.golfBallList.get(number - 1) != null) {
			golfBallCart.add(item.golfBallList.get(number - 1));
			itemCount++;
			cartChanged = true;
		}
	}
	
	/**
	 * Removes generic items from the shoppingCart
	 * @param number Item number in the list
	 * @param quant Quantity to remove
	 */
	public void removeFromCart(int number, String quant) {		
		if (number >= 1 && number <= shoppingCart.size()) {
			int cartQuantity = Integer.parseInt(shoppingCart.get(number - 1).getQuantity());
			if (Integer.parseInt(quant) >= 1 && Integer.parseInt(quant) <= cartQuantity) {
				String productID;
				for (Item product: item.itemList) {
					productID = product.getItemID();
					if (productID.equals(shoppingCart.get(number - 1).getItemID())) {
						product.setQuantity("" + (Integer.parseInt(product.getQuantity()) + Integer.parseInt(quant)));
					}
				}
				String newQuantity = "" + (cartQuantity - Integer.parseInt(quant));
				shoppingCart.get(number - 1).setQuantity(newQuantity);
				if (Integer.parseInt(shoppingCart.get(number - 1).getQuantity()) <= 0) {
					shoppingCart.remove(number - 1);
					itemCount--;
					cartChanged = true;
				}			
			} else {
				System.out.println("Invalid quantity");
			}
		} else if (itemCount == 0) {
			System.out.println("Your cart is empty.");
		} else {
			System.out.println("Invalid item choice");
		}
			
	}
	
	/**
	 * Removes a shirt from the shirtsCart
	 * @param number Item number in the list
	 */
	public void removeShirtFromCart(int number) {
		try {
			if (shirtsCart.get(number - 1) != null) {
				shirtsCart.remove(number - 1);
				itemCount--;
				cartChanged = true;
			}
		} catch (IndexOutOfBoundsException e) {
			System.out.println("Invalid item choice");
		}		
	}
	
	/**
	 * Removes a golf club from the golfClubCart
	 * @param number Item number in the list
	 */
	public void removeClubFromCart(int number) {
		try {
			if (golfClubCart.get(number - 1) != null) {
				golfClubCart.remove(number - 1);
				itemCount--;
				cartChanged = true;
			}
		} catch (IndexOutOfBoundsException e) {
			System.out.println("Invalid choice");
		}
		
	}
	
	/**
	 * Removes a golf ball from the golfBallCart
	 * @param number Item number in the list
	 */
	public void removeBallFromCart(int number) {
		try {
			if (golfBallCart.get(number - 1) != null) {
				golfBallCart.remove(number - 1);
				itemCount--;
				cartChanged = true;
			}
		} catch (IndexOutOfBoundsException e) {
			System.out.println("Invalid choice");
		}
		
	}
	
	/**
	 * Prints the current shopping cart
	 */
	public void printCart() {
		System.out.println("Shopping Cart: ");
		String itemDescription;
		int itemNumber = 1;
		for (Item item: shoppingCart) {
			itemDescription = itemNumber + ". " + item.toString();
			System.out.println(itemDescription);
			itemNumber++;
		}
		System.out.println("Shirts:");
		itemNumber = 1;
		for (poloShirt ps: shirtsCart) {
			itemDescription = itemNumber + ". " + ps.toString();
			System.out.println(itemDescription);
			itemNumber++;
		}
		System.out.println("Clubs:");
		itemNumber = 1;
		for (golfClub gc: golfClubCart) {
			itemDescription = itemNumber + ". " + gc.toString();
			System.out.println(itemDescription);
			itemNumber++;
		}
		System.out.println("Balls:");
		itemNumber = 1;
		for (golfBall gb: golfBallCart) {
			itemDescription = itemNumber + ". " + gb.toString();
			System.out.println(itemDescription);
			itemNumber++;
		}
	}
	
}
