package items;

/**
 * 
 * @author blank
 *
 */
public class poloShirt extends Shirt{//start of class
	
	//number of buttons variable created 
	private String numberOfButtons;
	
/**
 * 
 * @param size
 * @param color
 * @param price
 * @param numOfButtons
 */
	public poloShirt(String size, String color, String skueNum, String price, String numOfButtons) {//start of param
		
		//paramaters supered so that test class can inherit.
		super(size, color, "Polo", skueNum, price);
		
		//instance of buttons created
		this.numberOfButtons= numOfButtons;
		
	}//end of param

/**
 * 
 * @param numOfButtons
 */
	public void setnumButtons(String numOfButtons) {//start of field
		
		//initilizing instance of buttons
		this.numberOfButtons = numOfButtons;
		
	}//end of field
	/**
	 * 
	 * @return
	 */
	public String getnumButtons() {//start of field
		
		//returning buttons
		return this.numberOfButtons;
		
	}//end of field
	
	/**
	 * creating to string and using super so that the test class can read the toString 
	 */
	
	public String toString() {//start of toString
		
		//returing super with to String so that main can read buttons
		return super.toString() +   "      Skue #: " + getskueNum() +     "    price: $" + getPrice() + "      Number of Buttons:" + getnumButtons();
		
	}//end of toString

	

}
