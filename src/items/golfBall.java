package items;

/**
 *golfBall class stores stockNumber, golfBallName, golfBallMakee, and golfBallPrice. golfBallExtended class extends golfBall class.
 *
 */ 

public class golfBall extends Item

{ 
	// stockNumber, golfBallName, golfBallMakee, and golfBallPrice variables are declared for golfBall
	private String golfBallName, golfBallMake;

/**
 * @param stockNumber
 * @param golfBallName
 * @param golfBallMake
 * @param golfBallPrice
 */
public golfBall(String skueNum, String golfBallName, String golfBallMake, String price) 
{	
	
	super(skueNum, price);
	// constructor has parameters for golfBallName, golfBallMake, and golfBallPrice
  	this.golfBallName = golfBallName;
	this.golfBallMake = golfBallMake;
}

public String toString() 
{
	return "Stock Number: "+ getskueNum() + "      Golf Ball Name: " + getgolfBallName() + "       Golf Ball Make:     " 
			+ getgolfBallMake() + "               Golf Ball Price: $" +getPrice();
}
	/**
	 * @return the golfBallName
	 */
	public String getgolfBallName() 
	{
		return golfBallName;
	}
	
	/**
	 * @param golfBallName the golfBallName to set
	 */
	public void setgolfBallName(String golfBallName) 
	{
		this.golfBallName = golfBallName;
	}
	
	/**
	 * @return the golfBallMake
	 */
	public String getgolfBallMake() 
	{
		return golfBallMake;
	}
	
	/**
	 * @param golfBallMake the golfBallMake to set
	 */
	public void setgolfBallMake(String golfBallMake) 
	{
		this.golfBallMake = golfBallMake;
	}

}
