package items;

//file import to read file
import java.io.File;
//exception imported
import java.io.FileNotFoundException;
//arrayList imported
import java.util.ArrayList;
//scanner imported
import java.util.Scanner;

/**
 * 
 * @author blank
 *
 */
public class TangibleItemTestClass {//start of class
	
	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {//start of main
		
		//creating shirt arrayList for shirt object items o be stored into
		ArrayList<poloShirt>printShirt = new ArrayList<poloShirt>();
		
		//labeling the shirt section
		System.out.println("Golf Shirts:");
		
		//spacing from the golf shirts title.
		System.out.println("");
		
		//storing the shirt attribute objects into the printshirt arrayList
		printShirt = golfShirtsArrayList();
		
		//print shirt method called in order to print te shirt items. 
		printShirtMethod(printShirt);
		
		//spacing inbetween the shirt title given
		System.out.println("\n");
		
		//golfclub item label created 
		System.out.println("Golf Clubs :");
		
		//spacing between title and the items
		System.out.println("");
		
		//golfClub arraylist created in order for club items to be stored into and then printed later
		ArrayList<golfClubExtended>printGolfClub = new ArrayList<golfClubExtended>();
		
		//storing the club items into the printgolfclubarray
		printGolfClub=golfClubArray();
		
		//printing the golf club items
		printGolfClubMethod(printGolfClub);
		
		//spacing
		System.out.println("\n");
		
		//golf ball title created
		System.out.println("Golf Balls:");
		
		//spacing
		System.out.println("");
		
		//golfball arraylist created in order for ball items to be stored into and then printed later
		ArrayList<golfBallExtended>printGolfBall = new ArrayList<golfBallExtended>();
		
		//storing the ball items into the printgolfballarray
		printGolfBall=GolfBallItem();
		
		//printing the golf ball items
		printGolfBallMethod(printGolfBall);
		
	}//end of main
	/**
	 * void method to print the golf ball items
	 * @param printGolfBall
	 */
	private static void printGolfBallMethod(ArrayList<golfBallExtended> printGolfBall) {
		
		//enhanced forloop used to print array
		for(golfBallExtended golfBall:printGolfBall) {
			
		//Calling and printing toString for formatting
		System.out.println(golfBall.toString());
	}
}
	/**
	 * void method used in order to print the gold club items
	 * @param printGolfClub
	 */
	private static void printGolfClubMethod(ArrayList<golfClubExtended> printGolfClub) {
		//for loop to print the golf club arrayList out
	for(golfClubExtended golfClub : printGolfClub)				
	{		
		//printing out to String
			 System.out.println(golfClub.toString());			  
	}
		
	
}
	/**
	 * void method used in order to print the golf shirt items.
	 * @param printShirt
	 */
	private static void printShirtMethod(ArrayList<poloShirt> printShirt) {
	// TODO Auto-generated method stub
		
		//for loop to print the golf shirt arrayList out
		for(poloShirt poloshirt : printShirt) {
			
			//printing out to String
			System.out.println(poloshirt.toString());
		}
}
	/**
	 * ArrayList class created
	 * @return
	 */
	protected static ArrayList<poloShirt> golfShirtsArrayList(){//start of method
		//arrayList created
		ArrayList<poloShirt> shirtsarray = new ArrayList<poloShirt>();
		
		try {//start of try
			
					//file being stored into golfList
			      	File golfList = new File("golfShirts.txt");
			      
			      	//using scanner to read file
			      	Scanner lineReader = new Scanner(golfList);
			     
			      	//using while loop to read line by line
			      	while (lineReader.hasNext()){//start of while
			    	
			      	//storing lines of text into shirts string	
			        String shirts = lineReader.next();
			        
			        //splitting text and storing into array
			        String [] shirt = shirts.split("-");
			        
			       
			        //storing shirt size into string size
			        String size = shirt[0];
			        
			        //storing shirt color into string shirt
			        String color = shirt[1];
			       
			        //storing buttons into string shirt
			        String numOfButtons = shirt[2];
			        
			        //storin the skue number into String shirt
			        String skueNum = shirt[3];
			        
			        //storin the price number into String shirt
			        String price = shirt[4];
			        
			      //creating instance of a polo shirt and storing the shirt attributes into the paramaters from the textfile 
			        poloShirt poloshirt = new poloShirt(size, color,skueNum, price, numOfButtons);
			        
			        //ading the poloshirt object to the array list
			        shirtsarray.add(poloshirt);
			        
			  
			      }//end of while
			     //closing the scanner
			      lineReader.close();
			      //catch statement
			    }//end of try
					catch (FileNotFoundException e){//start of catch
			    //printing if file is not found	
			      System.out.println("An error occurred.");
			      e.printStackTrace();
			      
			    }//end of catch
		
		//forloop to print the arrayList out
		
			//returing shirtsArray ArrayList
			return shirtsarray;

			}//end of method
	

	/**
	 * ArrayList class created
	 * @return
	 */
	protected static ArrayList<golfClubExtended>golfClubArray()
	{   
		
		//arrayList created
		ArrayList<golfClubExtended> golfclubarray = new ArrayList<golfClubExtended>();
		try {
				//file being stored into golfList
			    File golfOrders = new File("golfClubs.txt");
			      
			    //using scanner to read file
			    Scanner lineReader = new Scanner(golfOrders);
			     
			    //using while loop to read line by line
			    while (lineReader.hasNext())
				{
				    	
				  //storing lines of text into shirts string	
				  String items = lineReader.next();
				  
				  String club[] = items.split("-");
				        
				  //splitting text and storing into array
				  String skueNum = club[0];
				  
				  String golfClubName = club[1];
				  
				  String golfClubMake = club[2];
				  
				  String  golfClubPrice = club[3];
				  
				  String golfClubLength= club[4];
					  // stores components of array into relevant variables
							         
					  //creating instance of a golf club and stores variables into parameters 
					  golfClubExtended golfClub = new golfClubExtended(skueNum, golfClubName, golfClubMake, golfClubPrice, golfClubLength);
							        
					  // golfclub object is added to the array list
					  golfclubarray.add(golfClub);	
					  
				    }
			      lineReader.close();
			}
		
		catch (FileNotFoundException e)
		{
			//printing if file is not found	
			System.out.println("An error occurred.");
			e.printStackTrace(); 
		}

		//returns the golfclubarray ArrayList
		return golfclubarray;


	}//end of method
	
	/**
	 * ArrayList class created
	 * @return
	 */
	
	
	
	protected static ArrayList<golfBallExtended> GolfBallItem()
	{   
		
		//arrayList created
		ArrayList<golfBallExtended> golfBallArray = new ArrayList<golfBallExtended>();
		try {
				//file being stored into golfList
			    File golfOrders = new File("golfBalls.txt");
			      
			    //using scanner to read file
			    Scanner lineReader = new Scanner(golfOrders);
			     
			    //using while loop to read line by line
			    while (lineReader.hasNext())
				{
				    	
				  //storing lines of text into shirts string	
				  String items = lineReader.next();
				  
				  String club[] = items.split("-");
				        
				  //splitting text and storing into array
				  String skueNum = club[0];
				  
				  String golfBallName= club[1];
				  
				  String golfBallMake = club[2];
				  
				  String  price = club[3];
				  
				  String golfBallSize= club[4];
					  // stores components of array into relevant variables
							         
					  //creating instance of a golf club and stores variables into parameters 
					  golfBallExtended golfBall = new golfBallExtended(skueNum, golfBallName, golfBallMake, price, golfBallSize);
							        
					  // golfclub object is added to the array list
					  golfBallArray.add(golfBall);	
				    }
			    lineReader.close();
			      
			}
		
		catch (FileNotFoundException e)
		{
			//printing if file is not found	
			System.out.println("An error occurred.");
			e.printStackTrace(); 
		}
		//returns the golfBallArray ArrayList
		return golfBallArray;


	}//end of method



}//end of class
