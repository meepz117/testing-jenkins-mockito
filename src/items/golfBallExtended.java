package items;

/** 
 * golfBallExtended class extends the golfBall class and stores the golfBallSize
 */

public class golfBallExtended extends golfBall
{	
	//size variable declared for golfBall
	private String golfBallSize;

	/**
	 * @param stockNumber
	 * @param golfBallName
	 * @param golfBallMake
	 * @param golfBallPrice
	 * @param golfBallSize
	 */
	public golfBallExtended(String skueNum, String golfBallName, String golfBallMake, String price, String golfBallSize) 
	{
		super(skueNum, golfBallName, golfBallMake, price);
		
		// sets the golfBallSize
		this.golfBallSize = golfBallSize;		
	}

	/**
	 * @param golfBallSize the golfBallSize to set
	 */
	public void setgolfBallSize(String golfBallSize) 
	{
		this.golfBallSize = golfBallSize;	
	}
	
	/**
	 * @return  the golfBallSize
	 */
	public String getgolfBallSize() 
	{
		return this.golfBallSize;		
	}
	
	/**
	 * toString() is created for golfBall class, displays stockNumber, golfBallName, golfBallMake, golfBallPrice, and golfBallSize
	 */
	public String toString() 
	{	
		return super.toString() + "     Golf Ball Size: " + getgolfBallSize();		
	}
}