package items;

/**
 * 
 */

/**
 * @author blank
 *
 */
public class Shirt extends Item{//start of class

//Creating shirt variables
private String shirtColor, shirtSize,shirtStyle;

/**
 * 
 * @param size
 * @param color
 * @param price
 * @param style
 */
public Shirt(String size, String color, String style, String skueNum,  String price){//start of field
	
	super(skueNum, price);
	
	this.shirtSize = size;
	this.shirtColor = color;
	this.shirtStyle = style;
	
}//end of field

/**
 * 
 */
public String toString() {// start of toString
	return "Shirt Size: " + getShirtSize() + "     Shirt Color: " + getShirtColor() +"      Shirt Style: " + getShirtStyle();
}// end of toString

/**
 * @return the shirtColor
 */
public String getShirtColor() {//start of field
	return shirtColor;
}//end of field

/**
 * @param shirtColor the shirtColor to set
 */
public void setShirtColor(String shirtColor) {//start of field
	this.shirtColor = shirtColor;
}//end of field





/**
 * @return the shirtSize
 */
public String getShirtSize() {//start of field
	return shirtSize;
}//end of field


/**
 * @param shirtSize the shirtSize to set
 */
public void setShirtSize(String shirtSize) {//start of field
	this.shirtSize = shirtSize;
}//end of field


/**
 * @return the shirtStyle
 */
public String getShirtStyle() {//start of field
	return shirtStyle;
}//end of field


/**
 * @param shirtStyle the shirtStyle to set
 */
public void setShirtStyle(String shirtStyle) {//start of field
	this.shirtStyle = shirtStyle;
}//end of field

}//end of method
