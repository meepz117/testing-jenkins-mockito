package address;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;



public class customerAddressDemo {
	
	   // String street, String zip, String city, String state
	   public static void main(String[] args) throws IOException, ClassNotFoundException {
	      
	       /**
	        *  Create 10 null addresses because we want to verify that the objects
	        * that we write and the objects that we retrieve are the same
	        */
	        
	       Address newAdd1 = null;
	       Address newAdd2 = null;
	       Address newAdd3 = null;
	       Address newAdd4 = null;
	       Address newAdd5 = null;
	       Address newAdd6 = null;
	       Address newAdd7 = null;
	       Address newAdd8 = null;
	       Address newAdd9 = null;
	       Address newAdd10 = null;
	      
	       /**
	        *  Create 10 valid addresses
	        */
	       Address add1 = new Address("Street1", "Z1", "C1", "State1");
	       Address add2 = new Address("Street2", "Z2", "C2", "State2");
	       Address add3 = new Address("Street3", "Z3", "C3", "State3");
	       Address add4 = new Address("Street4", "Z4", "C4", "State4");
	       Address add5 = new Address("Street5", "Z5", "C5", "State5");
	       Address add6 = new Address("Street6", "Z6", "C6", "State6");
	       Address add7 = new Address("Street7", "Z7", "C7", "State7");
	       Address add8 = new Address("Street8", "Z8", "C8", "State8");
	       Address add9 = new Address("Street9", "Z9", "C9", "State9");
	       Address add10 = new Address("Street10", "Z10", "C10", "State10");
	      
	       /**
	        *  serialize the addresses
	        */
	       FileOutputStream fos = new FileOutputStream("customer.txt");
	       ObjectOutputStream oos = new ObjectOutputStream(fos);
	       oos.writeObject(add1);
	       oos.writeObject(add2);
	       oos.writeObject(add3);
	       oos.writeObject(add4);
	       oos.writeObject(add5);
	       oos.writeObject(add6);
	       oos.writeObject(add7);
	       oos.writeObject(add8);
	       oos.writeObject(add9);
	       oos.writeObject(add10);
	      
	       /**
	        *  de-serialize the addresses
	        */
	       FileInputStream fis = new FileInputStream("customer.txt");
	       ObjectInputStream ois = new ObjectInputStream(fis);
	       newAdd1 = (Address) ois.readObject();
	       newAdd2 = (Address) ois.readObject();
	       newAdd3 = (Address) ois.readObject();
	       newAdd4 = (Address) ois.readObject();
	       newAdd5 = (Address) ois.readObject();
	       newAdd6 = (Address) ois.readObject();
	       newAdd7 = (Address) ois.readObject();
	       newAdd8 = (Address) ois.readObject();
	       newAdd9 = (Address) ois.readObject();
	       newAdd10 = (Address) ois.readObject();
	      
	       /**
	        *  print all the addresses
	        */
	      
	        System.out.println("Old address 1: " + add1);
	       System.out.println("New address 1: " + newAdd1);
	       newAdd1.prettyPrint();
	      
	       System.out.println("Old address 2: " + add2);
	       System.out.println("New address 2: " + newAdd2);
	       newAdd2.prettyPrint();

	       System.out.println("Old address 3: " + add3);
	       System.out.println("New address 3: " + newAdd3);
	       newAdd3.prettyPrint();

	       System.out.println("Old address 4: " + add4);
	       System.out.println("New address 4: " + newAdd4);
	       newAdd4.prettyPrint();

	       System.out.println("Old address 5: " + add5);
	       System.out.println("New address 5: " + newAdd5);
	       newAdd5.prettyPrint();

	       System.out.println("Old address 6: " + add6);
	       System.out.println("New address 6: " + newAdd6);
	       newAdd6.prettyPrint();

	       System.out.println("Old address 7: " + add7);
	       System.out.println("New address 7: " + newAdd7);
	       newAdd7.prettyPrint();

	       System.out.println("Old address 8: " + add8);
	       System.out.println("New address 8: " + newAdd8);
	       newAdd8.prettyPrint();

	       System.out.println("Old address 9: " + add9);
	       System.out.println("New address 9: " + newAdd9);
	       newAdd9.prettyPrint();

	       System.out.println("Old address 10: " + add10);
	       System.out.println("New address 10: " + newAdd10);
	       newAdd10.prettyPrint();

	       
	     
	   }

	}
	
	


