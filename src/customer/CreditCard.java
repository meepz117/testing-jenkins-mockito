package customer;
import java.util.Scanner;



public class CreditCard {
		
		/**
		 * List credit card fields
		 * issuer
		 * card number
		 * expiration date
		 */

		private  String issuer;
		private  String cardNumber;
		private String expirationDate;
		private String verification;
		public boolean verified = false;
		
		
		
		/**
		 * @param issuer
		 * @param cardNumber
		 * @param expirationDate
		 * @param verification
		 */
		public CreditCard(String issuer, String cardNumber, String expirationDate, String verification) {
			super();
			this.issuer = issuer;
			this.cardNumber = cardNumber;
			this.expirationDate = expirationDate;
			this.verification = verification;
		}
		/**
		 * Generate getters and setters
		 */
		
		
		/**
		 * @return the issuer
		 */
		public String getIssuer() {
			return issuer;
		}
		/**
		 * @param issuer the issuer to set
		 */
		public void setIssuer(String issuer) {
			this.issuer = issuer;
		}
		/**
		 * @return the cardNumber
		 */
		public String getCardNumber() {
			return cardNumber;
		}
		/**
		 * @param cardNumber the cardNumber to set
		 */
		public void setCardNumber(String cardNumber) {
			this.cardNumber = cardNumber;
		}
		/**
		 * @return the expirationDate
		 */
		public String getExpirationDate() {
			return expirationDate;
		}
		/**
		 * @param expirationDate the expirationDate to set
		 */
		public void setExpirationDate(String expirationDate) {
			this.expirationDate = expirationDate;
		}
		
		
		/**
		 * @return the verification
		 */
		public String getVerification() {
			return verification;
		}
		/**
		 * @param verification the verification to set
		 */
		public void setVerification(String verification) {
			this.verification = verification;
		}
		
		/**
		 * @return the verified
		 */
		public boolean isVerified() {
			return verified;
		}


		/**
		 * @param verified the verified to set
		 */
		public void setVerified(boolean verified) {
			this.verified = verified;
		}


		/**
		 * the authorize method determines whether 
		 * the card information entered is valid
		 */
		public void authorize(Customer customer) {
			
			String number = "";
			String date = "";
			String cvv2 = "";
			
			/**
			 * create scanner object to allow input
			 * promt user to enter card's information
			 */
			Scanner kybd = new Scanner(System.in);
			
			System.out.print("Enter Card Number (Format: xxxx-xxxx-xxxx-xxxx or xxxx-xxxxxx-xxxxx): " );
				number = kybd.nextLine();
				if (number.isEmpty()) {
					number = "";
				}
				
			System.out.println("Enter the card's expiration date (Format: mm/yy): ");
				date = kybd.nextLine();
				if (date.isEmpty()) {
					number = "";
				}
				
			System.out.println("Enter the card's CVV2 code: ");
				cvv2 = kybd.nextLine();
				if (cvv2.isEmpty()) {
					number = "";
				}
			
				
				
			
			/**
			 * check for length of card number
			 * check for valid characters
			 */
				if (number.length() == 19 && (customer.getCreditCard().getIssuer().equals("visa") || customer.getCreditCard().getIssuer().equals("mastercard") || customer.getCreditCard().getIssuer().equals("discovercard"))) {
					if (customer.getCreditCard().getCardNumber().contentEquals(number)) {
						if (customer.getCreditCard().getExpirationDate().contentEquals(date)) {
							if (customer.getCreditCard().getVerification().contentEquals(cvv2)) {
								verified = true;
							} else {
								System.out.println("Invalid cvv2 number");
							}
						} else {
							System.out.println("Invalid expiration date");
						}
					} else {
						System.out.println("Invalid credit card number");
					}
				} else if (number.length() == 17 && customer.getCreditCard().getIssuer().contentEquals("americanexpress")) {
					if (customer.getCreditCard().getCardNumber().contentEquals(number)) {
						if (customer.getCreditCard().getExpirationDate().contentEquals(date)) {
							if (customer.getCreditCard().getVerification().contentEquals(cvv2)) {
								verified = true;
							} else {
								System.out.println("Invalid cvv2 number");
							}
						} else {
							System.out.println("Invalid expiration date");
						}
					} else {
						System.out.println("Invalid credit card number");
					}
				} else {
					System.out.println("Credit card does not match issuer format or invalid issuer");
				}
			
			
			
			
		}//end method
		
		/**
		 * toString method returns fields as a String
		 */
		@Override
		public String toString() {
			return "CreditCard [issuer=" + issuer + ", cardNumber=" + cardNumber + ", expirationDate=" + expirationDate
					+ "]";
		}
		
		
		

		
	}//end class



