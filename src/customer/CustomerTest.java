package customer;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;
//import static org.mockito.Mockito.when;
import address.Address;

class CustomerTest {
	
	@Before
	public void setup() {
	}

	@Test
	void test() {
//		Customer customerMock = mock(Customer.class);
		Customer testCustomer;
		String customerId = "1234";
		String customerName = "Jason Knox";
		// mocking both address and cc
		CreditCard cc = new CreditCard("mastercard","2337-9190-1929-1401","01/23","548");
		Address address = mock(Address.class);
//		CreditCard cardMock = mock(CreditCard.class);
		
		when(address.getStreet()).thenReturn("123 jones street");
		when(address.getCity()).thenReturn("bristol"); 
		when(address.getState()).thenReturn("Wi");
		when(address.getZip()).thenReturn("18927");
		
		testCustomer = new Customer(customerId, customerName, address, cc);
		
		assertEquals(testCustomer.getId(), customerId);
		assertEquals(testCustomer.getCustomerName(), customerName);
		assertEquals(testCustomer.getShippingAddress(), address);
		assertEquals(testCustomer.getCreditCard(), cc);
		
		// just one way of using mockito, ensure certain methods are used
		// this is used so it will give us a pass
		// verify(address);
		//this is not used, so it will give us a failed test - "wanted but not invoked"
		verify(address).getState();
	}

}
