package customer;
/**
 * 
 */

/**
 * @author SAM
 *
 */
 

import java.util.ArrayList;

import java.util.Scanner;

import address.Address; //import riddhi's class


public class Customer{

	/**
	 * Declare Customer class fields
	 */
	
	private String idNumber;							//the customer's ID number
	private String customerName;						//the customer's name
	protected Address address;				//an Address object from riddhi's class
	private CreditCard creditCard; 				//the customer's card info
			

	
	
	/**
	 * Constructor Initializes a customer id, a customer name
	 * a place holder for credit card and creates an Address object
	 * 
	 * 
	 * 
	 * 
	 *@param id
	 * @param name
	 * @param address an Address object
	 * @param cc
	 */
	
	public Customer(String id, String name, Address address, CreditCard cc )
	{
		// John Edit: Fixed address and added this. to each variable
		this.idNumber = id;
		this.customerName = name; 
		this.address = address; 
		this.creditCard = cc;
	}
	
	
	/*
	 * Create Getters and Setters for each field
	 */
	
	/**
	 * getCreditCard method returns a Customer object's credit card information
	 * @return the creditCard
	 */
	public CreditCard getCreditCard() {
		return creditCard;
	}
	
	/**
	 * setCreditCard method stores a value into the creditCard field
	 * @param creditCard the creditCard to set
	 */
	public void setCreditCard(String creditCardNumber, String issuer, String expiration, String CVV2) {
		this.creditCard = new CreditCard(issuer, creditCardNumber, expiration, CVV2);
	}
	/**
	 * getId method returns a Customer object's ID number
	 * @return the id
	 */
	public String getId() {
		return idNumber;
	}
	/**
	 * setId method  stores a value in the id field
	 * @param id the id to set
	 */
	public void setIdNumber(String id) {
		this.idNumber = id;
	}
	/**
	 * getName method returns a Customer object's name
	 * @return the name
	 */
	public String getCustomerName() {
		return customerName;
	}
	/**
	 * setName method stores a value in the name field
	 * @param name the name to set
	 */
	public  void setCustomerName(String name) {
		this.customerName = name;
	}
	/**
	 * getShippingAddress method returns a copy of the address object
	 * @return address, an Address object
	 */
	public Address getShippingAddress() {
				
				return address;
	}
	
	
	
	
	/**
	 * The getById method searches through the custList ArrayList
	 *  for the customer object that has the entered ID number 
	 *  stored in its idNumber field
	 * @return customerName
	 */
	
	public Customer getById() {
		Customer customer = new Customer(idNumber, customerName, address, creditCard); 
		ArrayList<Customer> custList = new ArrayList<Customer>();  ///Create a Customer ArrayList
		
		Scanner scanner = new Scanner(System.in); //Create Scanner object for input
		String idSearch; //to hold an idSearch string
		
		//prompt the user to enter a customer's ID
		System.out.println("Enter the ID number of the Customer you want to search for: "); 
		idSearch = scanner.nextLine();
		
		
		/**
		 * For every customer object in the ArrayList
		 * if the Customer's ID isn't blank and is the same as the ID being searched
		 * the customer's name linked to the ID gets printed  
		 */
		for(int i = 0; i<custList.size(); i++) {
			if(custList.get(i).getId() != null && custList.get(i).getId().contentEquals(idSearch))
				System.out.println("The Customer with ID " + idSearch + " is named " + customer.getCustomerName());
		}
		
		return customer; //return the customer object
		
		
		
	}
	
	/**
	 * Provide toString method to return Customer as string
	 */
	
	@Override
	public String toString() {
		return "Customer idNumber " + idNumber + ", customerName " + customerName + ", shippingAddress "
				+ address +  ", creditCard " + creditCard + "]";
	}
	
	
	
}
