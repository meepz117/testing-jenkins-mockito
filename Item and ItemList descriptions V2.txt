Item Text File
	Each line contains the name, price and quantity of a product
	Use underscores instead of spaces for the name
	Separate the name, price and quantity using indents
Item Class
	Features:
		Variables for name, price and quantity
		Constructor for the name, price and quantity field
			For Name, it replaces the underscores with spaces
		Getters and Setters for name, price and quantity
ItemList Class
	Features:
		ArrayLists for item names, price and quantity
		Main method to perform certain required tasks on startup
		mapList method
			Uses a scanner to read the items.txt file
			Makes an Item class object to make sure the formatting for name is correct
			Adds the name, price and quantity obtained from the text file to the respective arraylists
			Continues till the end of the text file
		printList method
			Combines the data from the three arraylist and prints them to the console, one product per line
		add method
			Adds the defined product from the inputs to the arrayLists and the text file and prints
			the modified product list
		remove method
			Using the given item number, the method removes the selected product from both the arraylists and
			the text file and prints the modified product list
		count method
			Returns the number of products
		choices method
			Gives the use the choice between adding a product, removing a product and quitting the program
				Choice 1 (Add):
					Prompts you to enter the product name, price and quantity
					Runs the add method using the given inputs
				Choice 2 (Remove):
					Prompts you to enter the product number (the left most number in the printed list)
					Runs the remove method using the given inputs
				Choice 3 (Quit):
					Immediately quits the program
			Continues to run until the program is terminated, either by chosing choice 3 or force quitting
V2 Changelog
	Item Class
		Modified some of the validation code for setters and getters to ensure that the inputs passed through are completely valid
		Added a lineNumber variable as well as a setter and getter for it so I can have error messages display what line 
		in the text file it occurred in
		Added a toString method
	ItemList Class
		Decreased the number of ArrayLists to 1
		itemList now only accepts Item Class objects
		mapList method changes:
			Added currentLine string variable
			The method now splits each line of the text file using tabs as the delimiter and adds them to the string array, product.
			After checking if the product string array is the length of three, an Item Class Object is created using
			the three values found in the product array and once the values are validated the item object is added to the
			itemList. If, after the line from the text file is tokenized, the product array has a length less than or greater than 3
			the item is skipped and a message stating where the incomplete data is found in the text file is displayed
		printList method changes:
			Creates an object of Shawn's ShirtTestClass
			Added a string variable, itemDescription
			To print the arrayList:
				A for-each loop is now used
				itemDescription gets defined using the itemNumber and the new toString method of each Item object
				Afterwards, itemDescription is printed and itemNumber gets incremented
			Now uses the ShirtTestClass object to print out the shirt text file
		add method changes:
			Modified code due to the decreased number of ArrayLists
			Now takes only String values as inputs
		remove method changes:
			Added removedItem string, format of the string is set to match the lines of the text file
			Now only removes from itemList
			Modified the line removal process to check for both line number and whether currentLine matches removedItem
				Validation efforts made so if the line that matches the itemNumber does not match the removedItem, it will continue to search
				for that item until it is found
		choices method changes:
			quantity and price variables changed to string type
			modified the validation process a bit due to the quantity and price variable change
				